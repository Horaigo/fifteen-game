var field_array = [], field, row, column, size, step_count = 0, hours, minutes, seconds, clock;

function swapCells(field_array,i1,j1,i2,j2) {				
	t = field_array[i1][j1];
	field_array[i1][j1] = field_array[i2][j2];
	field_array[i2][j2] = t;
}

document.getElementById("size").onchange = newGame;

function cellClick(event) {
	var event = event || window.event,
		field_element = event.srcElement || event.target,
		i = field_element.id.charAt(0),
		j = field_element.id.charAt(2);
	if((i == row && Math.abs(j - column) == 1) || (j == column && Math.abs(i - row) == 1)) {
    step_count++;
    steps.innerText = "STEPS: "+step_count;					
		document.getElementById(row + " " + column).innerHTML = field_element.innerHTML;
		field_element.innerHTML = "";
		row = i;
		column = j;
		var victory = true;
		for(i = 0; i < size; ++i)
			for(j = 0; j < size; ++j)
				if(i + j != (size-1)*2 && document.getElementById(i + " " + j).innerHTML != i*size + j + 1) {
					victory = false;
					break;
				}
        if(victory) {
          alert("Congratulations!\n If you want to start a new game, click on the button at the top of the page.");
          clearInterval(clock);
        }
			}
}

function newGame() {
  if(clock>0)
    clearInterval(clock);
  hours = 0;
  minutes = 0;
  seconds = 0;
  startTime();
  step_count = 0;
  steps.innerText = "STEPS: "+step_count;
  size = getFieldSize();
  field_array = [];			
	for(i = 0; i < size; ++i) {
		field_array[i] = []
		for(j = 0; j < size; ++j){
			if(i + j != (size-1)*2)
				field_array[i][j] = i*size + j + 1;
			else
				field_array[i][j] = "";
		}
	}
	row = size-1;
	column = size-1;
	for(i = 0; i < 1600; ++i)
		switch(Math.round(3*Math.random())) {
      case 0: 
        if(row != 0) swapCells(field_array,row,column,--row,column); // up
        break;
      case 1: 
        if(column != size-1) swapCells(field_array,row,column,row, ++column); // right
        break;
      case 2: 
        if(row != size-1) swapCells(field_array,row,column,++row,column); // down
        break;
      case 3: 
        if(column != 0) swapCells(field_array,row,column,row,--column); // left
        break;
		}
	var table = document.createElement("table"),
		tbody = document.createElement("tbody");					
  table.appendChild(tbody);
  table.className = "play-table";
  field = document.getElementById("field");
  field_width = field.clientWidth;
  field_height = field.clientHeight;
	for(i = 0; i < size; ++i) {
		var table_row = document.createElement("tr");
		for(j = 0; j < size; ++j) {
			var cell = document.createElement("td");
				cell.id = i + " " + j;
				cell.onclick = cellClick;
        cell.innerHTML = field_array[i][j];
        cell.style.width = field_width/size+"px";
        cell.style.height = field_height/size-2.5+"px";
        cell.className = "playground-cell";
				table_row.appendChild(cell);
		}
		tbody.appendChild(table_row);					
	}
	if(field.childNodes.length == 1)
		field.removeChild(field.firstChild);	
	field.appendChild(table);	
}

function getFieldSize() {
  return Number(document.getElementById("size").value[0]);
}

function startTime() {
  if(seconds<59) {
    seconds++;
  }
  else {
    if(minutes<59) {
      minutes++;
      seconds = 0;
    }
    else {
      if(hours<23) {
        hours++
        minutes = 0;
      }
      else {
        seconds = 0;
        minutes = 0;
        hours = 0;
      }
    }
  }
  seconds = checkTime(""+seconds);
  minutes = checkTime(""+minutes);
  hours = checkTime(""+hours);
  document.getElementById('time').innerHTML="TIME: "+hours + ":" + minutes + ":" + seconds;
  clock=setTimeout('startTime()',1000);
}

function checkTime(i) {
  if (i<10 && i.length<2)
    i="0" + i;
  return i;
}